// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "Interactable.h"
#include "GameFramework/Actor.h"
#include "Water.generated.h"

UCLASS()
class SNAKEGAME_API AWater : public AActor, public IInteractable
{
	GENERATED_BODY()
	
public:	
	AWater();

protected:
	virtual void BeginPlay() override;

public:	
	virtual void Tick(float DeltaTime) override;
	virtual void Interact(AActor* Interactor, bool bIsHead) override;
};
