// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "SnakeBase.generated.h"

class ASnakeElementBase;
DECLARE_LOG_CATEGORY_EXTERN(LogSnake, Log, All);

UENUM()
enum class EMovementDirection {
	UP,
	DOWN,
	LEFT,
	RIGHT
};

UENUM()
enum class ESpeedModifier {
	SPEED_NORMAL,
	SPEED_WATER,
	SPEED_ACCELERATION
};

UCLASS()
class SNAKEGAME_API ASnakeBase : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ASnakeBase();

	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<ASnakeElementBase> SnakeElementClass;

	UPROPERTY(EditDefaultsOnly)
	float ElementSize;
	UPROPERTY(EditDefaultsOnly)
	float MovementSpeed;
	UPROPERTY()
	float CurrentSpeed;
	UPROPERTY(EditDefaultsOnly)
	float SlowFactor;
	UPROPERTY()
	TArray<ASnakeElementBase*> SnakeElements;
	UPROPERTY()
	EMovementDirection LastMoveDirection;
	UPROPERTY()
	int AmountElementsToAdd;
	
protected:
	virtual void BeginPlay() override;

public:	
	virtual void Tick(float DeltaTime) override;
	UFUNCTION(BlueprintCallable)
	void AddSnakeElement(int ElementsCount = 1);
	UFUNCTION(BlueprintCallable)
	void Move();
	UFUNCTION()
	void SnakeElementOverlap(ASnakeElementBase* OverlappedElement, AActor* Other);
	UFUNCTION()
	void UpdateMovementSpeed(ESpeedModifier SpeedModifier);
	UFUNCTION()
	void CleanSnake();

};
