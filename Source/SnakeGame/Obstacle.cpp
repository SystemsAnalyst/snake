// Fill out your copyright notice in the Description page of Project Settings.


#include "Obstacle.h"

#include "PlayerPawnBase.h"
#include "Kismet/GameplayStatics.h"
#include "SnakeBase.h"

// Sets default values
AObstacle::AObstacle(){
	PrimaryActorTick.bCanEverTick = true;
}

// Called when the game starts or when spawned
void AObstacle::BeginPlay(){
	Super::BeginPlay();
}

// Called every frame
void AObstacle::Tick(float DeltaTime){
	Super::Tick(DeltaTime);
}

void AObstacle::Interact(AActor* Interactor, bool bIsHead) {
	if(bIsHead) {
		auto Snake = Cast<ASnakeBase>(Interactor);
		if(IsValid(Snake)) {
			APawn* Pawn = UGameplayStatics::GetPlayerPawn(GetWorld(), 0);
			auto PlayerPawn = Cast<APlayerPawnBase>(Pawn);
			if(IsValid(PlayerPawn)) {
				PlayerPawn->KillSnakeActor();
			}
		}
	}
}

