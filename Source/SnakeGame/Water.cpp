// Fill out your copyright notice in the Description page of Project Settings.


#include "Water.h"

#include "SnakeBase.h"

AWater::AWater(){
	PrimaryActorTick.bCanEverTick = false;
}

void AWater::BeginPlay(){
	Super::BeginPlay();
}

void AWater::Tick(float DeltaTime){
	Super::Tick(DeltaTime);
}

void AWater::Interact(AActor* Interactor, bool bIsHead) {
	if(bIsHead) {
		auto Snake = Cast<ASnakeBase>(Interactor);
		if(IsValid(Snake)) {
			Snake->UpdateMovementSpeed(ESpeedModifier::SPEED_WATER);
		}
	}
}

