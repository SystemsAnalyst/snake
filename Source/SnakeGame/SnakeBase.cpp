// Fill out your copyright notice in the Description page of Project Settings.


#include "SnakeBase.h"
#include "SnakeElementBase.h"

DEFINE_LOG_CATEGORY(LogSnake);

ASnakeBase::ASnakeBase(){
	PrimaryActorTick.bCanEverTick = true;
	ElementSize = 5.0f;
	MovementSpeed = 1.0f;
	SlowFactor = 2.0f;
	CurrentSpeed = 1.0f;
	LastMoveDirection = EMovementDirection::UP;
}

void ASnakeBase::BeginPlay(){
	Super::BeginPlay();
	UpdateMovementSpeed(ESpeedModifier::SPEED_NORMAL);
	AddSnakeElement(4);
	SnakeElements[0]->SetFirstElementType();
}

void ASnakeBase::Tick(float DeltaTime){
	Super::Tick(DeltaTime);
	Move();
}

void ASnakeBase::AddSnakeElement(int ElementsCount) {
	// Если количество элементов == 0, то добавляю голову, иначе - в счетчик
	if(SnakeElements.Num() == 0) {
		// стартуем с нуля
		FVector NewLocation(FVector(0, 0 ,0));
		FTransform NewTransform = FTransform(GetActorLocation() - NewLocation);
		ASnakeElementBase* NewSnakeElem = GetWorld()->SpawnActor<ASnakeElementBase>(SnakeElementClass, NewTransform);
		NewSnakeElem->SnakeOwner = this;
		SnakeElements.Add(NewSnakeElem);
		ElementsCount --;
	}
	AmountElementsToAdd = ElementsCount;
}



void ASnakeBase::Move() {
	FVector MovementVector(0.,0.,0.);

	switch(LastMoveDirection) {
		case EMovementDirection::UP:
			MovementVector.X += ElementSize;
			break;
		case EMovementDirection::DOWN:
			MovementVector.X -= ElementSize;
			break;
		case EMovementDirection::LEFT:
			MovementVector.Y -= ElementSize;
			break;
		case EMovementDirection::RIGHT:
			MovementVector.Y += ElementSize;
	}
	// Нужно обновить модификатор, т.к. если мы движемся по траве, то изменения режима не происходит 
	UpdateMovementSpeed(ESpeedModifier::SPEED_NORMAL);
	
	SnakeElements[0]->ToggleCollision(false);
	FVector NewLocation = 	SnakeElements[0]->GetActorLocation() + MovementVector;
	for(int i = 0;  i<SnakeElements.Num(); i++ ) {
		FVector NextLocation = SnakeElements[i]->GetActorLocation();
		SnakeElements[i]->SetActorLocation(NewLocation);
		NewLocation = NextLocation; 
	}
	// NewLocation содержит координаты последнего элемента змейки
	if(AmountElementsToAdd > 0) {
		UE_LOG(LogSnake, Log, TEXT("AmountElementsToAdd, %i"), AmountElementsToAdd);
		ASnakeElementBase* NewSnakeElem = GetWorld()->SpawnActor<ASnakeElementBase>(SnakeElementClass, FTransform(NewLocation));
		NewSnakeElem->SnakeOwner = this;
		SnakeElements.Add(NewSnakeElem);
		AmountElementsToAdd --;
	}
	SnakeElements[0]->ToggleCollision(true);
}

void ASnakeBase::SnakeElementOverlap(ASnakeElementBase* OverlappedElement, AActor* Other) {
	if(IsValid(OverlappedElement)) {
		int32 ElementIndex;
		SnakeElements.Find(OverlappedElement,ElementIndex);
		bool bIsFirst = ElementIndex == 0;

		IInteractable* InteractableInterface = Cast<IInteractable>(Other);
		if(InteractableInterface) {
			InteractableInterface->Interact(this, bIsFirst);
		}
	}
}

void ASnakeBase::UpdateMovementSpeed(ESpeedModifier SpeedModifier) {
	switch (SpeedModifier) {
		case ESpeedModifier::SPEED_WATER:
			// Замедляем скорость движения
			CurrentSpeed = CurrentSpeed + (MovementSpeed * SlowFactor - CurrentSpeed)/2.0;
			break;
		case ESpeedModifier::SPEED_ACCELERATION:
			CurrentSpeed = CurrentSpeed / SlowFactor;
			break;
		case ESpeedModifier::SPEED_NORMAL:
			// Если движемся медленнее, чем базовая скорость, то с каждым шагом ускоряемся 
			if(CurrentSpeed > MovementSpeed) {
				CurrentSpeed -= (CurrentSpeed - MovementSpeed)/2.0f;
			}
	}
	GEngine->AddOnScreenDebugMessage(INDEX_NONE, 4.0f, FColor::Green, "NewSpeed: " + FString::FromInt(CurrentSpeed*100), true);
	SetActorTickInterval(CurrentSpeed);
}

void ASnakeBase::CleanSnake() {
	for(int i = SnakeElements.Num()-1;  i>=0; i-- ) {
		SnakeElements[i]->SnakeOwner = nullptr;
		SnakeElements[i]->Destroy();
	}
	SnakeElements.Empty();
}


