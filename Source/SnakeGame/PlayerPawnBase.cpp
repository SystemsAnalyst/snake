// Fill out your copyright notice in the Description page of Project Settings.


#include "PlayerPawnBase.h"

#include "SnakeBase.h"
#include "Camera/CameraComponent.h"
#include "Components/InputComponent.h"

APlayerPawnBase::APlayerPawnBase(){
	PrimaryActorTick.bCanEverTick = true;
	PawnCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("SnakePawnCamera"));
	RootComponent = PawnCamera;
}

void APlayerPawnBase::BeginPlay(){
	Super::BeginPlay();
	SetActorRotation(FRotator(-32,-3,0));
}

void APlayerPawnBase::Tick(float DeltaTime){
	Super::Tick(DeltaTime);
}

void APlayerPawnBase::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent){
	Super::SetupPlayerInputComponent(PlayerInputComponent);
	PlayerInputComponent->BindAxis("Vertical", this, &APlayerPawnBase::HandlePlayerVerticalInput);
	PlayerInputComponent->BindAxis("Horizontal", this, &APlayerPawnBase::HandlePlayerHorizontalInput);
}

void APlayerPawnBase::CreateSnakeActor() {
	SnakeActor = GetWorld()->SpawnActor<ASnakeBase>(SnakeActorClass, FTransform());
}

void APlayerPawnBase::KillSnakeActor_Implementation() {
	SnakeActor->CleanSnake();
	SnakeActor->Destroy();
}

void APlayerPawnBase::HandlePlayerVerticalInput(float Value) {
	if(IsValid(SnakeActor)) {
		if(Value >0 && SnakeActor->LastMoveDirection != EMovementDirection::DOWN) {
			SnakeActor->LastMoveDirection = EMovementDirection::UP;
		}
		else if(Value <0 && SnakeActor->LastMoveDirection != EMovementDirection::UP) {
			SnakeActor->LastMoveDirection = EMovementDirection::DOWN;
		}
	}
}

void APlayerPawnBase::HandlePlayerHorizontalInput(float Value) {
	if(IsValid(SnakeActor)) {
		if(Value >0 && SnakeActor->LastMoveDirection != EMovementDirection::RIGHT) {
//			UE_LOG(LogSnake, Log, TEXT("LEFT, %lf"), Value);
			SnakeActor->LastMoveDirection = EMovementDirection::LEFT;
		}
		else if(Value <0 && SnakeActor->LastMoveDirection != EMovementDirection::LEFT) {
//			UE_LOG(LogSnake, Log, TEXT("RIGHT, %lf"), Value);
			SnakeActor->LastMoveDirection = EMovementDirection::RIGHT;
		}
	}
}

