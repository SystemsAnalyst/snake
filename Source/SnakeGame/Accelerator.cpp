// Fill out your copyright notice in the Description page of Project Settings.


#include "Accelerator.h"

#include "SnakeBase.h"

// Sets default values
AAccelerator::AAccelerator(){
	PrimaryActorTick.bCanEverTick = true;
}

void AAccelerator::BeginPlay(){
	Super::BeginPlay();
}

void AAccelerator::Tick(float DeltaTime){
	Super::Tick(DeltaTime);

	FRotator ActorRotator = GetActorRotation();
	ActorRotator.Yaw+=2.0f;
	SetActorRotation(ActorRotator.Clamp());
}

void AAccelerator::Interact(AActor* Interactor, bool bIsHead) {
	if(bIsHead) {
		auto Snake = Cast<ASnakeBase>(Interactor);
		if(IsValid(Snake)) {
			Snake->UpdateMovementSpeed(ESpeedModifier::SPEED_ACCELERATION);
			this->Destroy();
		}
	}
}

